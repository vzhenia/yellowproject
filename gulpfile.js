const gulp = require('gulp');
const watch = require('gulp-watch');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const through = require('through2');
const uglify = require('gulp-uglify');
//added constants -> gulp-less
const less = require('gulp-less');
const cssmin = require('gulp-cssmin');
const autoprefixer = require('gulp-autoprefixer');
const postcss = require('gulp-postcss');
const rename = require('gulp-rename');


browserSync.init({
  server: {
    baseDir: "./dest/"
  }
});

function logFileHelpers() {
    return through.obj((file, enc, cb) => {
        console.log(file.babel.usedHelpers);
        cb(null, file);
    });
}

gulp.task('less', function() {
    return gulp.src('./src/styles/*.less')
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dest'));
});

gulp.task('js', function() {
  gulp.src('src/*.js')
    .pipe(sourcemaps.init())
      .pipe(babel())
      .pipe(logFileHelpers())
      .pipe(concat('all.js'))
      .pipe(uglify())
    .pipe(sourcemaps.write('./maps/'))
    .pipe(gulp.dest('dest'));
});

gulp.task('default', function() {
  gulp.watch('./dest/index.html').on('change', browserSync.reload);
  gulp.watch('src/*.js', function(event) {
      gulp.run('js');
      browserSync.reload();
  });
  gulp.watch('./**/*.less', ['less']);
});
